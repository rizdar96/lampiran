<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alternatif_model extends CI_Model
{

    public function get($params = [])
    {
        if (isset($params['pelamar_id'])) {
            $this->db->where('pelamar.id', $params['pelamar_id']);
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = null;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['belum_input_alternatif'])) {
            $this->db->where('alternatif.pendidikan_id IS NULL', null);
            $this->db->or_where('alternatif.tes_tertulis_id IS NULL', null);
            $this->db->or_where('alternatif.pengalaman IS NULL', null);
            $this->db->or_where('alternatif.usia IS NULL', null);
            $this->db->or_where('alternatif.tes_fisik IS NULL', null);
        }

        $this->db->select('pelamar.id, pelamar.nik, pelamar.nama, pelamar.tempat_lahir, pelamar.tanggal_lahir, pelamar.alamat');
        $this->db->select('
         alternatif.id as alternatif_id, alternatif.pelamar_id, alternatif.pendidikan_id, alternatif.tes_tertulis_id,
         alternatif.hasil, alternatif.pengalaman, alternatif.usia, alternatif.tes_fisik
         ');
        $this->db->select('pendidikan.id as pendidikan_id, pendidikan.jenjang, pendidikan.bobot as bobot_pendidikan');
        $this->db->select('tes_tertulis.id as tes_tertulis_id, tes_tertulis.hasil_tes as hasil_tes_tertulis, tes_tertulis.bobot as bobot_tes_tertulis');

        $this->db->join('alternatif', 'alternatif.pelamar_id = pelamar.id', 'left');
        $this->db->join('pendidikan', 'pendidikan.id = alternatif.pendidikan_id', 'left');
        $this->db->join('tes_tertulis', 'tes_tertulis.id = alternatif.tes_tertulis_id', 'left');

        $alt = $this->db->get('pelamar');
        if (isset($params['pelamar_id'])) {
            return $alt->row_array();
        }
        return $alt->result_array();
    }

    public function save($params = [])
    {
        if (isset($params['pelamar_id'])) {
            $this->db->set('pelamar_id', $params['pelamar_id']);
        }

        if (isset($params['pendidikan_id'])) {
            $this->db->set('pendidikan_id', $params['pendidikan_id']);
        }

        if (isset($params['tes_tertulis_id'])) {
            $this->db->set('tes_tertulis_id', $params['tes_tertulis_id']);
        }

        if (isset($params['usia'])) {
            $this->db->set('usia', $params['usia']);
        }

        if (isset($params['pengalaman'])) {
            $this->db->set('pengalaman', $params['pengalaman']);
        }

        if (isset($params['tes_fisik'])) {
            $this->db->set('tes_fisik', $params['tes_fisik']);
        }

        if (isset($params['id'])) {
            $this->db->where('id', $params['id']);
            $this->db->update('alternatif');
        } else {
            $this->db->insert('alternatif');
        }
    }

    public function delete($id = null)
    {
        $this->db->where('pelamar_id', $id);
        $this->db->delete('alternatif');
    }

}

/* End of file Alternatif_model.php */
/* Location: ./application/models/Alternatif_model.php */
