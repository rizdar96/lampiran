<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    function get($params)
    {
        if (isset($params['email'])) {
            $this->db->where('user_email', $params['email']);
        }

        $user = $this->db->get('users');

        if (isset($params['id'])) {
            return $user->row_array();
        }

        return $user->result_array();
    }
}
