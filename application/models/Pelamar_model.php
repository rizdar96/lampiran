<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelamar_model extends CI_Model
{
    public function get($params = [])
    {
        if (isset($params['id'])) {
            $this->db->where('id', $params['id']);
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = null;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        $this->db->select('id, nik, nama, tempat_lahir, tanggal_lahir, alamat, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS usia');

        $pelamar = $this->db->get('pelamar');

        if (isset($params['id'])) {
            return $pelamar->row_array();
        }

        return $pelamar->result_array();
    }

    public function save($params = [])
    {
        if (isset($params['nik'])) {
            $this->db->set('nik', $params['nik']);
        }
        if (isset($params['nama'])) {
            $this->db->set('nama', $params['nama']);
        }
        if (isset($params['tempat_lahir'])) {
            $this->db->set('tempat_lahir', $params['tempat_lahir']);
        }
        if (isset($params['tanggal_lahir'])) {
            $this->db->set('tanggal_lahir', $params['tanggal_lahir']);
        }
        if (isset($params['alamat'])) {
            $this->db->set('alamat', $params['alamat']);
        }

        if (isset($params['id'])) {
            $this->db->where('id', $params['id']);
            $this->db->update('pelamar');
        } else {
            $this->db->insert('pelamar');
        }
    }


    public function delete($id = null)
    {
        $this->db->where('id', $id);
        $this->db->delete('pelamar');
    }
}
