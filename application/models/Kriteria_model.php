<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kriteria_model extends CI_Model
{

    public function getPendidikan($params = [])
    {
        $this->db->order_by('bobot', 'desc');
        return $this->db->get('pendidikan')->result_array();
    }

    public function savePendidikan($params = [])
    {
        if (isset($params['jenjang'])) {
            $this->db->set('jenjang', $params['jenjang']);
        }

        if (isset($params['bobot'])) {
            $this->db->set('bobot', $params['bobot']);
        }
        if (isset($params['id'])) {
            $this->db->where('id', $params['id']);
            $this->db->update('pendidikan');
        } else {
            $this->db->insert('pendidikan');
        }
    }

    public function deletePendidikan($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->delete('pendidikan');
        }
    }

    public function getTesTerulis($params = [])
    {
        $this->db->order_by('bobot', 'desc');
        return $this->db->get('tes_tertulis')->result_array();
    }

    public function saveTesTerulis($params = [])
    {
        if (isset($params['hasil_tes'])) {
            $this->db->set('hasil_tes', $params['hasil_tes']);
        }

        if (isset($params['bobot'])) {
            $this->db->set('bobot', $params['bobot']);
        }
        if (isset($params['id'])) {
            $this->db->where('id', $params['id']);
            $this->db->update('tes_tertulis');
        } else {
            $this->db->insert('tes_tertulis');
        }
    }

    public function deleteTesTerulis($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $this->db->delete('tes_tertulis');
        }
    }

}

/* End of file Kriteria_model.php */
/* Location: ./application/models/Kriteria_model.php */
