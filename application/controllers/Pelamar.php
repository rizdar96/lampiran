<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelamar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn')) {
            $this->session->set_flashdata('error', 'Login first');
            redirect('auth/login');
        }
        $this->load->model(['Pelamar_model', 'Alternatif_model']);
    }

    public function index()
    {
        $this->load->library('pagination');
        $config['base_url']             = site_url('pelamar');
        $config['total_rows']           = count($this->Pelamar_model->get());
        $config['per_page']             = 10;
        $config['page_query_string']    = true;
        $config['query_string_segment'] = 'offset';
        $this->pagination->initialize($config);

        $params['limit']  = 10;
        $params['offset'] = $this->input->get('offset', true);
        $data['pelamar'] = $this->Pelamar_model->get($params);
        $data['title']    = 'Data Pelamar';
        $data['page']     = 'pelamar/list';
        $this->load->view('templates/layout', $data);
    }

    public function detail($id = null)
    {
        if (is_null($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('pelamar');
        }

        $pelamar = $this->Pelamar_model->get(['id' => $id]);
        if (!$pelamar) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('pelamar');
        }
        $data['title']    = 'Detail Pelamar';
        $data['pelamar'] = $pelamar;
        $data['page']     = 'pelamar/detail';
        $this->load->view('templates/layout', $data);
    }

    public function tambah($id = null)
    {
        $title = is_null($id) ? 'Tambah Pelamar' : 'Edit Pelamar';
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nik', 'NIK', 'trim|required');
            $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
            $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'trim|required');
            $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            if ($this->form_validation->run()) {
                $params       = $this->input->post();
                $params['id'] = $id;
                $this->Pelamar_model->save($params);
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('pelamar');
            }
        }

        if (!is_null($id)) {
            $pelamar = $this->Pelamar_model->get(['id' => $id]);
            if (!$pelamar) {
                $this->session->set_flashdata('error', 'Data tidak ditemukan');
                redirect('pelamar');
            }
            $data['pelamar'] = $pelamar;
        }

        $data['title'] = $title;
        $data['page']  = 'pelamar/form';
        $this->load->view('templates/layout', $data);
    }

    public function hapus()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'ID', 'required|trim');
        if ($this->form_validation->run()) {
            $id = $this->input->post('id');
            $this->Alternatif_model->delete($id);
            $this->Pelamar_model->delete($id);
            redirect('pelamar');
        }else{
            redirect('pelamar');
        }
    }
}
