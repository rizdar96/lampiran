<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penilaian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn')) {
            $this->session->set_flashdata('error', 'Login first');
            redirect('auth/login');
        }
        $this->load->model(['Alternatif_model']);
    }

    public function index()
    {

        $alternatif = $this->Alternatif_model->get(['belum_input_alternatif' => true]);
        if (count($alternatif) > 0) {
            $data['penduduk'] = $alternatif;
            $data['page']     = 'penilaian/error';
        } else {

            $alternatif = $this->Alternatif_model->get();

            $pendidikan   = $this->config->item('pendidikan');
            $tes_tertulis = $this->config->item('tes_tertulis');
            $pengalaman   = $this->config->item('pengalaman');
            $usia         = $this->config->item('usia');
            $tes_fisik    = $this->config->item('tes_fisik');

            $bobotPendidikan  = $pendidikan['bobot'];
            $bobotTesTertulis = $tes_tertulis['bobot'];
            $bobotPengalaman  = $pengalaman['bobot'];
            $bobotUsia        = $usia['bobot'];
            $bobotTesFisik    = $tes_fisik['bobot'];

            $totalSemuaBobot = $bobotPendidikan + $bobotTesTertulis + $bobotPengalaman + $bobotUsia + $bobotTesFisik;

            $benCostPendidikan  = $pendidikan['kategori'] == 'benefit' ? 1 : -1;
            $benCostTesTertulis = $tes_tertulis['kategori'] == 'benefit' ? 1 : -1;
            $benCostPengalaman  = $pengalaman['kategori'] == 'benefit' ? 1 : -1;
            $benCostUsia        = $usia['kategori'] == 'benefit' ? 1 : -1;
            $benCostTesFisik    = $tes_fisik['kategori'] == 'benefit' ? 1 : -1;

            $data['bobot_pendidikan']   = $bobotPendidikan;
            $data['bobot_tes_tertulis'] = $bobotTesTertulis;
            $data['bobot_pengalaman']   = $bobotPengalaman;
            $data['bobot_usia']         = $bobotUsia;
            $data['bobot_tes_fisik']    = $bobotTesFisik;

            $data['normalisasi_bobot_pendidikan']   = $bobotPendidikan / $totalSemuaBobot;
            $data['normalisasi_bobot_tes_tertulis'] = $bobotTesTertulis / $totalSemuaBobot;
            $data['normalisasi_bobot_pengalaman']   = $bobotPengalaman / $totalSemuaBobot;
            $data['normalisasi_bobot_usia']         = $bobotUsia / $totalSemuaBobot;
            $data['normalisasi_bobot_tes_fisik']    = $bobotTesFisik / $totalSemuaBobot;

            $data['normalisasi_bobot_pendidikan_ben_cost']   = $data['normalisasi_bobot_pendidikan'] * $benCostPendidikan;
            $data['normalisasi_bobot_tes_tertulis_ben_cost'] = $data['normalisasi_bobot_tes_tertulis'] * $benCostTesTertulis;
            $data['normalisasi_bobot_pengalaman_ben_cost']   = $data['normalisasi_bobot_pengalaman'] * $benCostPengalaman;
            $data['normalisasi_bobot_usia_ben_cost']         = $data['normalisasi_bobot_usia'] * $benCostUsia;
            $data['normalisasi_bobot_tes_fisik_ben_cost']    = $data['normalisasi_bobot_tes_fisik'] * $benCostTesFisik;
            $data['total_semua_bobot']                       = $totalSemuaBobot;

            $total_s = 0;

            $s = [];

            foreach ($alternatif as $idx => $alt) {
                $no      = $idx + 1;
                $nilai_s =
                    ($alt['bobot_pendidikan'] ** $data['normalisasi_bobot_pendidikan_ben_cost']) *
                    ($alt['bobot_tes_tertulis'] ** $data['normalisasi_bobot_tes_tertulis_ben_cost']) *
                    ($alt['pengalaman'] ** $data['normalisasi_bobot_pengalaman_ben_cost']) *
                    ($alt['usia'] ** $data['normalisasi_bobot_usia_ben_cost']) *
                    ($alt['tes_fisik'] ** $data['normalisasi_bobot_tes_fisik_ben_cost']);

                $alternatif[$idx]['s'] = [
                    'name'    => 'S' . $no,
                    'nilai_s' =>
                    "(" . $alt['bobot_pendidikan'] . "<sup>" . $data['normalisasi_bobot_pendidikan_ben_cost'] . "</sup>)" .
                    " (" . $alt['bobot_tes_tertulis'] . "<sup>" . $data['normalisasi_bobot_tes_tertulis_ben_cost'] . "</sup>)" .
                    " (" . $alt['pengalaman'] . "<sup>" . $data['normalisasi_bobot_pengalaman_ben_cost'] . "</sup>)" .
                    " (" . $alt['usia'] . "<sup>" . $data['normalisasi_bobot_usia_ben_cost'] . "</sup>)" .
                    " (" . $alt['tes_fisik'] . "<sup>" . $data['normalisasi_bobot_tes_fisik_ben_cost'] . "</sup>)" .
                    " = " . $nilai_s,
                    's'       => $nilai_s,
                ];
                $total_s += $nilai_s;
            }

            foreach ($alternatif as $idx => $alt) {
                $no                    = $idx + 1;
                $nilai_v               = $alt['s']['s'] / $total_s;
                $alternatif[$idx]['v'] = [
                    'name'    => 'V' . $no,
                    'nilai_v' => $alt['s']['s'] . "/" . $total_s . ' = ' . $nilai_v,
                    'v'       => $nilai_v,
                ];
            }

            $data['alternatif'] = $alternatif;

            usort($alternatif, function ($a, $b) {
                if ($a['v']['v'] == $b['v']['v']) {
                    return 0;
                }

                return $a['v']['v'] < $b['v']['v'] ? 1 : -1;
            });

            $data['hasil_akhir_alternatif'] = $alternatif;

            $data['page'] = 'penilaian/hasil';
        }

        $data['title'] = 'Penilaian';
        $this->load->view('templates/layout', $data);
    }

}

/* End of file Penilaian.php */
/* Location: ./application/controllers/Penilaian.php */
