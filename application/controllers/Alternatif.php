<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alternatif extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn')) {
            $this->session->set_flashdata('error', 'Login first');
            redirect('auth/login');
        }
        $this->load->model(['Alternatif_model', 'Pelamar_model', 'Kriteria_model']);
    }

    public function index()
    {
        $this->load->library('pagination');
        $config['base_url']             = site_url('alternatif');
        $config['total_rows']           = count($this->Alternatif_model->get());
        $config['per_page']             = 10;
        $config['page_query_string']    = true;
        $config['query_string_segment'] = 'offset';
        $this->pagination->initialize($config);

        $params['limit']    = 10;
        $params['offset']   = $this->input->get('offset', true);
        $data['alternatif'] = $this->Alternatif_model->get($params);
        $data['title']      = 'Alternatif';
        $data['page']       = 'alternatif/list';
        $this->load->view('templates/layout', $data);
    }

    public function tentukan($id = null)
    {
        if (is_null($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('alternatif');
        }
        $pelamar = $this->Alternatif_model->get(['pelamar_id' => $id]);

        if (!$pelamar) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('alternatif');
        }

        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pendidikan_id', 'Pendidikan', 'required|trim');
            $this->form_validation->set_rules('tes_tertulis_id', 'Hasil Tes Tertulis', 'required|trim');
            $this->form_validation->set_rules('usia', 'Usia', 'required|trim');
            $this->form_validation->set_rules('pengalaman', 'Pengalaman', 'required|trim');
            $this->form_validation->set_rules('tes_fisik', 'Hasil Tes Fisik', 'required|trim');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            if ($this->form_validation->run()) {

                $params['id']              = $pelamar['alternatif_id'];
                $params['pelamar_id']      = $pelamar['id'];
                $params['pendidikan_id']   = $this->input->post('pendidikan_id');
                $params['tes_tertulis_id'] = $this->input->post('tes_tertulis_id');
                $params['usia']            = $this->input->post('usia');
                $params['pengalaman']      = $this->input->post('pengalaman');
                $params['tes_fisik']       = $this->input->post('tes_fisik');

                $this->Alternatif_model->save($params);
                $this->session->set_flashdata('success', 'Berhasil menyimpan data');
                redirect('alternatif');
            }
        }

        $data['pendidikan']   = $this->Kriteria_model->getPendidikan();
        $data['tes_tertulis'] = $this->Kriteria_model->getTesTerulis();
        $data['pelamar']      = $pelamar;
        $data['title']        = 'Tentukan Alternatif';
        $data['page']         = 'alternatif/form';
        $this->load->view('templates/layout', $data);
    }

}

/* End of file Alternatif.php */
/* Location: ./application/controllers/Alternatif.php */
