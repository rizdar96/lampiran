<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $user = $this->User_model->get(['email' => $email]);

            if (is_null($user)) {
                $this->session->set_flashdata('error', 'Username or Password did not match');
                redirect('auth/login');
            }
            if (!password_verify($password, $user[0]['user_password'])) {
                $this->session->set_flashdata('error', 'Username or Password did not match');
                redirect('auth/login');
            }
            
            $user[0]['isLoggedIn'] = true;
            $this->session->set_userdata($user[0]);
            redirect('');
        }

        $this->load->view('auth/login');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('success', 'Logout success');
        redirect('auth/login');

    }
}
