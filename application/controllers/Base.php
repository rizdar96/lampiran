<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Base extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn'))
        {
            $this->session->set_flashdata('error', 'Login first');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
		$this->load->view('templates/layout', $data);
    }
}
