<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kriteria extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn')) {
            $this->session->set_flashdata('error', 'Login first');
            redirect('auth/login');
        }
        $this->load->model(['Kriteria_model']);
    }

    public function index()
    {
        $data['pendidikan'] = $this->Kriteria_model->getPendidikan();
        $data['tes_tertulis'] = $this->Kriteria_model->getTesTerulis();
        $data['title']        = 'Kriteria';
        $data['page']         = 'kriteria/list';
        $this->load->view('templates/layout', $data);
    }

    public function pendidikan()
    {
        if(!$this->input->post()){
            $this->session->set_flashdata('error', 'URL tidak ditemukan.');
            redirect('kriteria');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('jenjang', 'Jenjang', 'required|trim');
        $this->form_validation->set_rules('bobot', 'Bobot', 'required|trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if ($this->form_validation->run()) {
            $params['id'] = $this->input->post('id') == 'x' ? null : $this->input->post('id');
            $params['jenjang'] = $this->input->post('jenjang');
            $params['bobot'] = $this->input->post('bobot');
            
            $this->Kriteria_model->savePendidikan($params);

            $this->session->set_flashdata('success', 'Kriteria pendidikan berhasil disimpan');
            redirect('kriteria#pendidikan');
        }
    }

    public function hapus_pendidikan()
    {
        if(!$this->input->post()){
            $this->session->set_flashdata('error', 'URL tidak ditemukan.');
            redirect('kriteria');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'ID', 'required|trim');
        if ($this->form_validation->run()) {
            $this->Kriteria_model->deletePendidikan($this->input->post('id'));
            $this->session->set_flashdata('success', 'Kriteria pendidikan berhasil dihapus');
            redirect('kriteria#pendidikan');
        }
    }

    public function tes_tertulis()
    {
        if(!$this->input->post()){
            $this->session->set_flashdata('error', 'URL tidak ditemukan.');
            redirect('kriteria');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('hasil_tes', 'Jenjang', 'required|trim');
        $this->form_validation->set_rules('bobot', 'Bobot', 'required|trim');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if ($this->form_validation->run()) {
            $params['id'] = $this->input->post('id') == 'x' ? null : $this->input->post('id');
            $params['hasil_tes'] = $this->input->post('hasil_tes');
            $params['bobot'] = $this->input->post('bobot');
            
            $this->Kriteria_model->saveTesTerulis($params);

            $this->session->set_flashdata('success', 'Kriteria tes tertulis berhasil disimpan');
            redirect('kriteria#testertulis');
        }
    }

    public function hapus_tes_tertulis()
    {
        if(!$this->input->post()){
            $this->session->set_flashdata('error', 'URL tidak ditemukan.');
            redirect('kriteria');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'ID', 'required|trim');
        if ($this->form_validation->run()) {
            $this->Kriteria_model->deleteTesTerulis($this->input->post('id'));
            $this->session->set_flashdata('success', 'Kriteria tes tertulis dihapus');
            redirect('kriteria#testertulis');
        }
    }

}

/* End of file Kriteria.php */
/* Location: ./application/controllers/Kriteria.php */
