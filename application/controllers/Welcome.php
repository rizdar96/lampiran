<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('isLoggedIn')) {
			redirect('auth/login');
		}
	}
	public function index()
	{
		$data['title'] = 'Selamat Datang';
		$data['page'] = 'welcome_message';
		$this->load->view('templates/layout', $data);
	}

	function form()
	{
		$data['title'] = 'Form';
		$data['page'] = 'templates/form';
		$this->load->view('templates/layout', $data);
	}
}
