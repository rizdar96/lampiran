<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config['pendidikan'] = [
    'bobot'    => 2,
    'kategori' => 'benefit',
];

$config['tes_tertulis'] = [
    'bobot'    => 2,
    'kategori' => 'benefit',
];

$config['pengalaman'] = [
    'bobot'    => 4,
    'kategori' => 'benefit',
];

$config['usia'] = [
    'bobot'    => 4,
    'kategori' => 'cost',
];

$config['tes_fisik'] = [
    'bobot'    => 3,
    'kategori' => 'benefit',
];
