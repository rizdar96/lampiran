<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('navActive')) {
    function navActive($uri = "")
    {
        $CI = &get_instance();
        if ($CI->uri->segment(1) == $uri) {
            return 'active';
        }
        return null;
    }
}

if (!function_exists('dd')) {
    function dd($text = "")
    {
        echo '<pre>';
        print_r($text);
        echo '</pre>';
        die();
    }
}

if (! function_exists('rupiahSeparator'))
{
    function rupiahSeparator($num = '')
    {
        if (!$num) {
            return '';
        }
        return 'Rp. ' . number_format($num, 0, '.', '.') . ',-';
    }
}

if (! function_exists('getAgeByDate'))
{
    function getAgeByDate($date = null)
    {
        if(is_null($date)) return null;

        return date_diff(date_create($date), date_create('now'))->y;
    }
}