<div class="card card-primary card-outline">
	<div class="card-body">

		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<h4>Pendidikan (Benefit)</h4>
					</div>
					<div class="col-md-6">
						<button class="btn btn-primary btn-xs float-right" id="addPendidikan"><i class="fa fa-plus"></i> Tambah</button>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Kriteria</th>
								<th>Bobot</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($pendidikan as $row): ?>
								<tr>
									<td><?php echo $row['jenjang'] ?></td>
									<td><?php echo $row['bobot'] ?></td>
									<td>
										<button class="btn btn-primary btn-xs" onclick="editPendidikan(<?php echo $row['id'] ?>, '<?php echo $row['jenjang'] ?>', <?php echo $row['bobot'] ?>)"><i class="fa fa-edit"></i></button>
										<button class="btn btn-danger btn-xs" onclick="deletePendidikan(<?php echo $row['id'] ?>, '<?php echo $row['jenjang'] ?>')"><i class="fa fa-trash"></i></button>
									</td>
								</tr>
							<?php endforeach?>
						</tbody>
					</table>
				</div>

			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<h4>Tes Tertulis (Benefit)</h4>
					</div>
					<div class="col-md-6">
						<button class="btn btn-primary btn-xs float-right" id="addTesTertulis"><i class="fa fa-plus"></i> Tambah</button>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Kriteria</th>
								<th>Bobot</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($tes_tertulis as $row): ?>
								<tr>
									<td><?php echo $row['hasil_tes'] ?></td>
									<td><?php echo $row['bobot'] ?></td>
									<td>
										<button class="btn btn-primary btn-xs" onclick="editTesTertulis(<?php echo $row['id'] ?>, '<?php echo $row['hasil_tes'] ?>', <?php echo $row['bobot'] ?>)"><i class="fa fa-edit"></i></button>
										<button class="btn btn-danger btn-xs" onclick="deleteTesTertulis(<?php echo $row['id'] ?>, '<?php echo $row['hasil_tes'] ?>')"><i class="fa fa-trash"></i></button>
									</td>
								</tr>
							<?php endforeach?>
						</tbody>
					</table>
				</div>

			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h4>Pengalaman (Benefit)</h4>
				<p>Kriteria pengalaman ini akan diisi bebas dan ditandai sebagai <strong>Benefit</strong>. Sebagai contoh isi kolom ini adalah <strong>10</strong> tahun</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h4>Usia (Cost)</h4>
				<p>Kriteria pengalaman ini akan diisi bebas dan ditandai sebagai <strong>Cost</strong>. Sebagai contoh isi kolom ini adalah <strong>10</strong> tahun</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h4>Skor Tes Fisik (Benefit)</h4>
				<p>Kriteria skor tes fisik ini akan diisi bebas dan ditandai sebagai <strong>Benefit</strong>. Sebagai contoh isi kolom ini adalah <strong>70</strong></p>
			</div>
		</div>

	</div>
</div>


<div class="modal fade" id="modalPendidikan">
	<div class="modal-dialog">
		<?php echo form_open(site_url('kriteria/pendidikan')); ?>
		<input type="hidden" name="id" id="idPendidikan">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modal-title-pendidikan"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jenjang Pendidikan</label>
							<input type="text" name="jenjang" class="form-control" id="jenjangPendidikan" required>
						</div>
						<div class="form-group">
							<label>Bobot</label>
							<input type="text" name="bobot" class="form-control" id="bobotPendidikan" required>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="modalDeletePendidikan">
	<div class="modal-dialog">
		<?php echo form_open(site_url('kriteria/hapus_pendidikan')); ?>
		<input type="hidden" name="id" id="idPendidikanDelete">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hapus Kriteria</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger text-center">Anda yakin akan menghapus Kriteria <span id="jenjangPendidikanDelete"></span>?</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-danger">Hapus</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="modalTesTertulis">
	<div class="modal-dialog">
		<?php echo form_open(site_url('kriteria/tes_tertulis')); ?>
		<input type="hidden" name="id" id="idTesTertulis">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modal-title-tes-tertulis"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Hasil Tes Tertulis</label>
							<input type="text" name="hasil_tes" class="form-control" id="hasilTesTertulis" required>
						</div>
						<div class="form-group">
							<label>Bobot</label>
							<input type="text" name="bobot" class="form-control" id="bobotTesTertulis" required>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div class="modal fade" id="modalDeleteTesTertulis">
	<div class="modal-dialog">
		<?php echo form_open(site_url('kriteria/hapus_tes_tertulis')); ?>
		<input type="hidden" name="id" id="idTesTertulisDelete">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hapus Kriteria</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger text-center">Anda yakin akan menghapus Kriteria <span id="hasilTesTertulisDelete"></span>?</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-danger">Hapus</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$("#addPendidikan").click(function(event) {
		$("#idPendidikan").val('x')	
		$("#jenjangPendidikan").val('')	
		$("#bobotPendidikan").val('')	
		$("#modal-title-pendidikan").html('Tambah Kriteria Pendidikan')
		$("#modalPendidikan").modal('show')
	});

	function editPendidikan(id, jenjang, bobot) {
		$("#modal-title-pendidikan").html('Edit Kriteria Pendidikan')
		$("#idPendidikan").val(id)	
		$("#jenjangPendidikan").val(jenjang)	
		$("#bobotPendidikan").val(bobot)	
		$("#modalPendidikan").modal('show')	
	}

	function deletePendidikan(id, jenjang) {
		$("#idPendidikanDelete").val(id)	
		$("#jenjangPendidikanDelete").html(jenjang)	
		$("#modalDeletePendidikan").modal('show')	
	}

	$("#addTesTertulis").click(function(event) {
		$("#idTesTertulis").val('x')	
		$("#hasilTesTertulis").val('')	
		$("#bobotTesTertulis").val('')	
		$("#modal-title-tes-tertulis").html('Tambah Kriteria Tes Tertulis')
		$("#modalTesTertulis").modal('show')
	});

	function editTesTertulis(id, jenjang, bobot) {
		$("#modal-title-tes-tertulis").html('Edit Kriteria Tes Tertulis')
		$("#idTesTertulis").val(id)	
		$("#hasilTesTertulis").val(jenjang)	
		$("#bobotTesTertulis").val(bobot)	
		$("#modalTesTertulis").modal('show')	
	}

	function deleteTesTertulis(id, hasilTes) {

		$("#idTesTertulisDelete").val(id)	
		$("#hasilTesTertulisDelete").html(hasilTes)	
		$("#modalDeleteTesTertulis").modal('show')	
	}
</script>