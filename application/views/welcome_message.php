<div class="card card-primary card-outline">
	<div class="card-header">
		<h5 class="m-0">Di Aplikasi Sistem Penunjang Keputusan Penerimaan Anggota Satuan Pengaman</h5>
	</div>
	<div class="card-body">
		<h6 class="card-title">
			Aplikasi ini adalah Aplikasi yang dirancang untuk skripsi dengan judul: <br>
			<strong> Sistem Penunjang Keputusan Penerimaan Anggota Satuan Pengaman Pada Perusahaan Outsourcing dengan Metode WP di PT. Global Security Service</strong>
		</h6>

		<p class="card-text">Sebagai syarat untuk meraih gelar Strata Satu pada STMIK Eresha</p>
		<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
	</div>
</div>