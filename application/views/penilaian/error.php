<div class="card card-primary card-outline">
	<div class="card-header">
		<div class="float-right">
		</div>
	</div>
	<div class="card-body">

		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger text-center">
					<h3>Penilaian belum bisa dilakukan karena ada data alternatif yang belum diisi. Silakan isi <strong>alternatif</strong> terlebih dahulu.</h3>
				</div>

			</div>
		</div>

	</div>
</div>