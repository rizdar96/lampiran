<div class="card card-primary card-outline">
	<div class="card-header">
		<div class="float-right">
		</div>
	</div>
	<div class="card-body">

		<div class="row">
			<div class="col-md-12">

				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#collapseAlternatif">Alternatif</a>
							</h4>
						</div>
						<div id="collapseAlternatif" class="panel-collapse collapse show">
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-hover" id="alternatif-dt">
										<thead>
											<tr>
												<td rowspan="2" class="text-center align-middle"><strong>NIK</strong></td>
												<td rowspan="2" class="text-center align-middle"><strong>Nama Pelamar</strong></td>
												<td colspan="5" class="text-center"><strong>Kriteria</strong></td>
												<td rowspan="2" class="text-center align-middle"><strong>Aksi</strong></td>
											</tr>
											<tr>
												<td class="text-center"><strong>Pendidikan</strong></td>
												<td class="text-center"><strong>Usia</strong></td>
												<td class="text-center"><strong>Pengalaman</strong></td>
												<td class="text-center"><strong>Hasil Tes Tertulis</strong></td>
												<td class="text-center"><strong>Hasil Tes Fisik</strong></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($alternatif as $row): ?>
												<tr>
													<td><?php echo $row['nik'] ?></td>
													<td><?php echo $row['nama'] ?></td>
													<td><?php echo $row['jenjang'] ?></td>
													<td><?php echo $row['usia'] ?></td>
													<td><?php echo $row['pengalaman'] ?></td>
													<td><?php echo $row['bobot_tes_tertulis'] ?></td>
													<td><?php echo $row['tes_fisik'] ?></td>
													<td>
														<?php if (!$row['jenjang'] || !$row['pengalaman'] || !$row['hasil_tes_tertulis'] || !$row['tes_fisik']) {?>
															<a href="<?php echo site_url('alternatif/tentukan/' . $row['id']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Tentukan Kriteria</a>
														<?php } else {?>
															<a href="<?php echo site_url('alternatif/tentukan/' . $row['id']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit</a>
														<?php }?>
													</td>
												</tr>
											<?php endforeach?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<hr>

				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#collapsePerhitungan">Perhitungan</a>
							</h4>
						</div>
						<div id="collapsePerhitungan" class="panel-collapse collapse">
							<div class="panel-body">
								<h4>Tahap 1 : Mencari Nilai W</h4>
								<br>
								<h5>Bobot Tiap Kriteria</h5>
								<strong>W :[ <?php echo $this->config->item('pendidikan')['bobot'] ?>, <?php echo $this->config->item('tes_tertulis')['bobot'] ?>, <?php echo $this->config->item('pengalaman')['bobot'] ?>, <?php echo $this->config->item('usia')['bobot'] ?>, <?php echo $this->config->item('tes_fisik')['bobot'] ?> ]</strong>
								<br>
								<br>
								<h5>Normalisasi Bobot W</h5>
								<p>
									<strong>W1 = <?php echo $bobot_pendidikan . ' / ' . $total_semua_bobot . ' = ' . $normalisasi_bobot_pendidikan ?></strong>
								</p>
								<p>
									<strong>W2 = <?php echo $bobot_tes_tertulis . ' / ' . $total_semua_bobot . ' = ' . $normalisasi_bobot_tes_tertulis ?></strong>
								</p>
								<p>
									<strong>W3 = <?php echo $bobot_pengalaman . ' / ' . $total_semua_bobot . ' = ' . $normalisasi_bobot_pengalaman ?></strong>
								</p>
								<p>
									<strong>W4 = <?php echo $bobot_usia . ' / ' . $total_semua_bobot . ' = ' . $normalisasi_bobot_usia ?></strong>
								</p>
								<p>
									<strong>W5 = <?php echo $bobot_tes_fisik . ' / ' . $total_semua_bobot . ' = ' . $normalisasi_bobot_tes_fisik ?></strong>
								</p>
								<br>
								<h5>Normalisasi berdasarkan keuntungan dan biaya</h5>
								<p>
									<strong>W1 = <?php echo $normalisasi_bobot_pendidikan_ben_cost ?></strong>
								</p>
								<p>
									<strong>W2 = <?php echo $normalisasi_bobot_tes_tertulis_ben_cost ?></strong>
								</p>
								<p>
									<strong>W3 = <?php echo $normalisasi_bobot_pengalaman_ben_cost ?></strong>
								</p>
								<p>
									<strong>W4 = <?php echo $normalisasi_bobot_usia_ben_cost ?></strong>
								</p>
								<p>
									<strong>W5 = <?php echo $normalisasi_bobot_tes_fisik_ben_cost ?></strong>
								</p>
								<hr>
								<h5>Tahap 2 : Mencari Nilai S</h5>
								<?php foreach ($alternatif as $row): ?>
									<p>
										<strong>
											<?php echo $row['s']['name'] . ' = ' . $row['s']['nilai_s'] ?></p>
										</strong>
									</p>
								<?php endforeach?>

								<hr>
								<h5>Tahap 3 : Mencari Nilai V</h5>
								<?php foreach ($alternatif as $row): ?>
									<p>
										<strong>
											<?php echo $row['v']['name'] . ' = ' . $row['v']['nilai_v'] ?></p>
										</strong>
									</p>
								<?php endforeach?>
							</div>
						</div>
					</div>
				</div>

				<hr>
				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#collapseHasil">Hasil</a>
							</h4>
						</div>
						<div id="collapseHasil" class="panel-collapse collapse show">
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-hover" id="hasil-dt">
										<thead>
											<th>Nomor</th>
											<th>Alternatif</th>
											<th>Nilai</th>
										</thead>
										<tbody>
											<?php $no = 1;foreach ($hasil_akhir_alternatif as $row): ?>
											<tr>
												<td><?php echo $no ?></td>
												<td><?php echo $row['nama'] ?></td>
												<td><?php echo $row['v']['v'] ?></td>
											</tr>
											<?php $no++;endforeach?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/datatables/datatables.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables/datatables.min.js') ?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#alternatif-dt").DataTable();
		$("#hasil-dt").DataTable();
	});
</script>