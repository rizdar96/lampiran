<div class="card card-primary card-outline">
	<div class="card-body">
		<?php echo validation_errors() ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">

					<table class="table table-hover">
						<tbody>
							<tr>
								<td>Nama</td>
								<td>: <?php echo $pelamar['nama'] ?></td>
							</tr>
							<tr>
								<td>NIK</td>
								<td>: <?php echo $pelamar['nik'] ?></td>
							</tr>
							<tr>
								<td>Tempat Lahir</td>
								<td>: <?php echo $pelamar['tempat_lahir'] ?></td>
							</tr>
							<tr>
								<td>Tanggal Lahir</td>
								<td>: <?php echo $pelamar['tanggal_lahir'] ?></td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>: <?php echo $pelamar['alamat'] ?></td>
							</tr>

						</tbody>
					</table>

				</div>
			</div>
		</div>
		<?php echo form_open(current_url()) ?>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label>Pendidikan Terakhir</label>
					<select name="pendidikan_id" class="form-control" required>
						<option value="">Pilih Pendidikan Terakhir</option>
						<?php foreach ($pendidikan as $row): ?>
							<option value="<?php echo $row['id'] ?>" <?php echo $row['id'] == $pelamar['pendidikan_id'] ? 'selected' : null ?>><?php echo $row['jenjang'] ?> <em>(Bobot <?php echo $row['bobot'] ?>)</em></option>
						<?php endforeach?>
					</select>
				</div>
				<div class="form-group">
					<label>Hasil Tes Tertulis</label>
					<select name="tes_tertulis_id" class="form-control" required>
						<option value="">Pilih Hasil Tes Tertulis</option>
						<?php foreach ($tes_tertulis as $row): ?>
							<option value="<?php echo $row['id'] ?>" <?php echo $row['id'] == $pelamar['tes_tertulis_id'] ? 'selected' : null ?>><?php echo $row['hasil_tes'] ?> <em>(Bobot <?php echo $row['bobot'] ?>)</em></option>
						<?php endforeach?>
					</select>
				</div>
				<div class="form-group">
					<label>Usia</label>
					<input type="number" name="usia" required class="form-control" value="<?php echo getAgeByDate($pelamar['tanggal_lahir']) ?>">
				</div>
				<div class="form-group">
					<label>Pengalaman</label>
					<input type="number" name="pengalaman" required class="form-control" value="<?php echo  $pelamar['pengalaman'] ?>">
				</div>
				<div class="form-group">
					<label>Hasil Tes Fisik</label>
					<input type="number" name="tes_fisik" required class="form-control" value="<?php echo $pelamar['tes_fisik'] ?>">
				</div>
			</div>
			<div class="col-md-4">
				<label for="">Aksi</label>
				<br>
				<button type="submit" class="btn btn-primary btn-block">Simpan</button>
				<a href="<?php echo site_url('/pelamar') ?>" type="submit" class="btn btn-warning btn-block">Batal</a>
			</div>
		</div>
		<?php echo form_close() ?>
	</div>
</div>

<?php $this->load->view('templates/datepicker');?>