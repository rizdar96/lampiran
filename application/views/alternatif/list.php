<div class="card card-primary card-outline">
	<div class="card-header">
		<div class="float-right">
		</div>
	</div>
	<div class="card-body">

		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<td rowspan="2" class="text-center align-middle"><strong>NIK</strong></td>
						<td rowspan="2" class="text-center align-middle"><strong>Nama Pelamar</strong></td>
						<td colspan="5" class="text-center"><strong>Kriteria</strong></td>
						<td rowspan="2" class="text-center align-middle"><strong>Aksi</strong></td>
					</tr>
					<tr>
						<td class="text-center"><strong>Pendidikan</strong></td>
						<td class="text-center"><strong>Usia</strong></td>
						<td class="text-center"><strong>Pengalaman</strong></td>
						<td class="text-center"><strong>Hasil Tes Tertulis</strong></td>
						<td class="text-center"><strong>Hasil Tes Fisik</strong></td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($alternatif as $row): ?>
						<tr>
							<td><?php echo $row['nik'] ?></td>
							<td><?php echo $row['nama'] ?></td>
							<td><?php echo $row['jenjang'] ?></td>
							<td><?php echo $row['usia'] ?></td>
							<td><?php echo $row['pengalaman'] ?></td>
							<td><?php echo $row['bobot_tes_tertulis'] ?></td>
							<td><?php echo $row['tes_fisik'] ?></td>
							<td>
								<?php if (!$row['jenjang'] || !$row['pengalaman'] || !$row['hasil_tes_tertulis'] || !$row['tes_fisik']) {?>
									<a href="<?php echo site_url('alternatif/tentukan/' . $row['id']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Tentukan Kriteria</a>
								<?php } else {?>
									<a href="<?php echo site_url('alternatif/tentukan/' . $row['id']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Edit</a>
								<?php }?>
							</td>
						</tr>
					<?php endforeach?>
				</tbody>
			</table>
			<nav aria-label="Page navigation example">
				<?php echo $this->pagination->create_links() ?>
			</nav>
		</div>

	</div>
</div>