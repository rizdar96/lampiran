<div class="card card-primary card-outline">
  <div class="card-header">
    <div class="float-right">
      <a href="<?php echo site_url('pelamar') ?>" class="btn btn-primary btn-xs"><i class="fa fa-arrow-left"></i> Kembali</a>
    </div>
  </div>
  <div class="card-body">

    <div class="table-responsive">

      <table class="table table-hover">
        <tbody>
          <tr>
            <td>Nama</td>
            <td>: <?php echo $pelamar['nama'] ?></td>
          </tr>
          <tr>
            <td>NIK</td>
            <td>: <?php echo $pelamar['nik'] ?></td>
          </tr>
          <tr>
            <td>Tempat Lahir</td>
            <td>: <?php echo $pelamar['tempat_lahir'] ?></td>
          </tr>
          <tr>
            <td>Tanggal Lahir</td>
            <td>: <?php echo $pelamar['tanggal_lahir'] ?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>: <?php echo $pelamar['alamat'] ?></td>
          </tr>

        </tbody>
      </table>

    </div>
  </div>
</div>