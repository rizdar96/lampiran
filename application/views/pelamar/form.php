<div class="card card-primary card-outline">
	<div class="card-body">
		<?php echo validation_errors() ?>
		<?php echo form_open(current_url()) ?>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" value="<?php echo isset($pelamar) ? $pelamar['nik'] : set_value('nik') ?>">
				</div>
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" value="<?php echo isset($pelamar) ? $pelamar['nama'] : set_value('nama') ?>">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" value="<?php echo isset($pelamar) ? $pelamar['tempat_lahir'] : set_value('tempat_lahir') ?>">
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<div class="input-group date" id="datepicker" data-target-input="nearest">
						<input type="text" name="tanggal_lahir" class="form-control datetimepicker-input datepicker" data-target="#datepicker" value="<?php echo isset($pelamar) ? $pelamar['tanggal_lahir'] : set_value('tanggal_lahir') ?>" />
					</div>
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<textarea name="alamat" class="form-control"><?php echo isset($pelamar) ? $pelamar['alamat'] : set_value('alamat') ?></textarea>
				</div>
			</div>
			<div class="col-md-4">
				<label for="">Aksi</label>
				<br>
				<button type="submit" class="btn btn-primary btn-block">Simpan</button>
				<a href="<?php echo site_url('/pelamar') ?>" type="submit" class="btn btn-warning btn-block">Batal</a>
			</div>
		</div>
		<?php echo form_close() ?>
	</div>
</div>

<?php $this->load->view('templates/datepicker');?>