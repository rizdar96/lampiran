<div class="card card-primary card-outline">
	<div class="card-header">
		<div class="float-right">
			<a href="<?php echo site_url('pelamar/tambah') ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Tambah</a>
		</div>
	</div>
	<div class="card-body">

		<div class="table-responsive">

			<table class="table table-striped table-hover">
				<thead>
					<th>NIK</th>
					<th>Nama Pelamar</th>
					<th>Usia</th>
					<th>Aksi</th>
				</thead>
				<tbody>
					<?php foreach ($pelamar as $row): ?>
						<tr>
							<td><?php echo $row['nik'] ?></td>
							<td><?php echo $row['nama'] ?></td>
							<td><?php echo $row['usia'] . ' Tahun' ?></td>
							<td>
								<a href="<?php echo site_url('pelamar/detail/' . $row['id']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> </a>
								<a href="<?php echo site_url('pelamar/edit/' . $row['id']) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> </a>
								<a class="btn btn-xs btn-danger" data-toggle="modal" onclick="setId(<?php echo $row['id'] ?>)" href='#modalHapus'><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					<?php endforeach?>
				</tbody>
			</table>
			<nav aria-label="Page navigation example">
				<?php echo $this->pagination->create_links() ?>
			</nav>
		</div>
	</div>
</div>


<div class="modal fade" id="modalHapus">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hapus Data Pelamar</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger text-center">
					Anda yakin akan menghapus data ini?
				</div>
			</div>
			<div class="modal-footer">
				<?php echo form_open(site_url('/pelamar/hapus')); ?>
				<input type="hidden" name="id" id="id">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-danger">Hapus</button>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function setId(id){
		$("#id").val(id);
	}
</script>