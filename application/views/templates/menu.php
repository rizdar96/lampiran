<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?php echo base_url() ?>" class="brand-link">
    <img src="<?php echo base_url('assets/img/AdminLTELogo.png') ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">SPK WP</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo base_url('assets/img/user1-128x128.jpg') ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><?php echo $this->session->userdata('user_full_name') ?></a>
      </div>
    </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

        <li class="nav-item">
          <a href="<?php echo site_url('') ?>" class="nav-link <?php echo navActive('') ?>">
            <i class="nav-icon fas fa-home "></i>
            <p>
              Beranda
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?php echo site_url('pelamar') ?>" class="nav-link <?php echo navActive('pelamar') ?>">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Pelamar
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?php echo site_url('kriteria') ?>" class="nav-link <?php echo navActive('kriteria') ?>">
            <i class="nav-icon fas fa-sliders-h"></i>
            <p>
              Kriteria
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?php echo site_url('alternatif') ?>" class="nav-link <?php echo navActive('alternatif') ?>">
            <i class="nav-icon fas fa-atom"></i>
            <p>
              Alternatif
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?php echo site_url('penilaian') ?>" class="nav-link <?php echo navActive('penilaian') ?>">
            <i class="nav-icon fas fa-balance-scale"></i>
            <p>
              Penilaian
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?php echo site_url('auth/logout') ?>" class="nav-link">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
              Logout
              <!-- <span class="right badge badge-danger">New</span> -->
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>