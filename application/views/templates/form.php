<div class="card card-primary card-outline">
    <div class="card-header">
        <h5 class="m-0">Featured</h5>
    </div>
    <div class="card-body">
        <?php echo form_open(current_url()) ?>
        <div class="row">
            <div class="col-md-8">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="">Aksi</label>
                <br>
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                <a href="<?php echo site_url('/') ?>" type="submit" class="btn btn-warning btn-block">Batal</a>
            </div>
        </div>
        <?php echo form_close() ?>
    </div>
</div>