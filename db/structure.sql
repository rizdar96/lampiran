-- MySQL Script generated by MySQL Workbench
-- Sat 07 Nov 2020 11:50:59 WIB
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema penerimaan_security
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_email` VARCHAR(45) NULL,
  `user_password` VARCHAR(100) NULL,
  `user_full_name` VARCHAR(100) NULL,
  `user_created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `user_updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pelamar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pelamar` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(100) NULL,
  `nik` VARCHAR(45) NULL,
  `tempat_lahir` VARCHAR(45) NULL,
  `tanggal_lahir` VARCHAR(45) NULL,
  `alamat` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pendidikan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pendidikan` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `jenjang` VARCHAR(100) NULL,
  `bobot` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tes_tertulis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tes_tertulis` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hasil_tes` VARCHAR(200) NULL,
  `bobot` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `alternatif`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `alternatif` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `pelamar_id` INT NULL,
  `hasil` DOUBLE NULL,
  `pendidikan_id` INT NULL,
  `tes_tertulis_id` INT NULL,
  `pengalaman` DOUBLE NULL,
  `usia` INT NULL,
  `tes_fisik` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_alternatif_penduduk_idx` (`pelamar_id` ASC),
  INDEX `fk_alternatif_pendidikan1_idx` (`pendidikan_id` ASC),
  INDEX `fk_alternatif_tes_tertulis1_idx` (`tes_tertulis_id` ASC),
  CONSTRAINT `fk_alternatif_penduduk`
    FOREIGN KEY (`pelamar_id`)
    REFERENCES `pelamar` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alternatif_pendidikan1`
    FOREIGN KEY (`pendidikan_id`)
    REFERENCES `pendidikan` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
  CONSTRAINT `fk_alternatif_tes_tertulis1`
    FOREIGN KEY (`tes_tertulis_id`)
    REFERENCES `tes_tertulis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
